





# Software Licenses Required

Please make sure you have the licenses for the following items. 
While they might not be listed as hard dependencies, they're quite useful and you'll probably need them.

- Simulink
- Aerospace Blockset
- Aerospace Toolbox
- Control System Toolbox
- Signal Processing Toolbox
- Simulink 3-D Animation

# Debugging

The Simulink Support Package for Parrot Minidrones is particularly helpful.
I would like to draw attention to the section "Run on Target Hardware" - it's quite useful in explaining how code generation is done and how to do it.



